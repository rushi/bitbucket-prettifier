# Bitbucket Prettifier

CSS improvements to Bitbucket's theme. Add in styles in style.css via Google Chrome's [Stylebot Extension](https://chrome.google.com/webstore/detail/stylebot/oiaejidbmkiecgbjeifoejpgmdaleoha?hl=en)

## Screenshot

![Screenshot](https://bitbucket.org/rushi/bitbucket_prettifier/raw/81e75551774424c3724d6336ada8ffba13af2148/screenshot.png)

## TODO

Greasemonkey & Tampermonkey compatible scripts for Firefox and Chrome.